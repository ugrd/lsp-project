package seeder

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/lsp-project/domain/constant"
	"gitlab.com/ugrd/lsp-project/domain/entity"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/pkg/security"
)

// UserSeeder is a struct
type UserSeeder struct{}

// Seed is a method to run seeder
func (r *UserSeeder) Seed(ctx context.Context, repo *registry.RepoRegistry) error {
	resp, _ := repo.RoleRepo.GetByCode(ctx, constant.Admin.String())
	if resp == nil {
		return fmt.Errorf("error seed: role admin not found")
	}

	const (
		// name is a constant default user's name
		name = "Jay"
		// username is a constant default user's username
		username = "achjailani"
	)

	userResp, _ := repo.UserRepo.GetByUsername(ctx, username)
	if userResp != nil {
		return nil
	}

	user := &entity.User{
		RoleID:   resp.ID,
		Name:     name,
		Username: username,
		Password: security.HashMake(fmt.Sprintf("achjailaniX@123")),
	}

	err := repo.UserRepo.Create(ctx, user)
	if err != nil {
		return fmt.Errorf("error seed: %v", err)
	}

	return nil
}

var _ Seed = &RoleSeeder{}
