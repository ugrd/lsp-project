package seeder

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/lsp-project/domain/constant"
	"gitlab.com/ugrd/lsp-project/domain/entity"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
)

// RoleSeeder is a struct
type RoleSeeder struct{}

// Seed is a method to run seeder
func (r *RoleSeeder) Seed(ctx context.Context, repo *registry.RepoRegistry) error {
	roles := []entity.Role{
		{
			Name:        constant.Admin.String(),
			Code:        constant.Admin.String(),
			Description: fmt.Sprintf("-"),
		},
		{
			Name:        constant.User.String(),
			Code:        constant.User.String(),
			Description: fmt.Sprintf("-"),
		},
	}

	for _, row := range roles {
		resp, _ := repo.RoleRepo.GetByCode(ctx, row.Code)
		if resp != nil {
			continue
		}

		err := repo.RoleRepo.Create(ctx, &row)
		for err != nil {
			return err
		}
	}

	return nil
}

var _ Seed = &RoleSeeder{}
