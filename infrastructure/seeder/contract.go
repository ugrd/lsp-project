package seeder

import (
	"context"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
)

// Seed is a contract
type Seed interface {
	Seed(ctx context.Context, repo *registry.RepoRegistry) error
}
