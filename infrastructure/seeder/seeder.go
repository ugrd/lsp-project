package seeder

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
)

// Seeder is a struct
type Seeder struct {
	repo *registry.RepoRegistry
}

// NewSeeder is a constructor
func NewSeeder(repo *registry.RepoRegistry) *Seeder {
	return &Seeder{repo: repo}
}

// Seed is a method to run seeders
func (sd *Seeder) Seed(seeds ...Seed) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	defer cancel()

	for _, seeder := range seeds {
		if err := seeder.Seed(ctx, sd.repo); err != nil {
			return fmt.Errorf("error seed: %v", err)
		}
	}

	return nil
}
