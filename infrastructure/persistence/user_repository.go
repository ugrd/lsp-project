package persistence

import (
	"context"
	"gitlab.com/ugrd/lsp-project/domain/entity"
	"gitlab.com/ugrd/lsp-project/domain/repository"

	"gorm.io/gorm"
)

// UserRepo is a constructor
type UserRepo struct {
	db *gorm.DB
}

// NewUserRepo is a constructor
func NewUserRepo(db *gorm.DB) *UserRepo {
	return &UserRepo{db}
}

var _ repository.UserRepositoryInterface = &UserRepo{}

// Create is a method to create new record
func (u UserRepo) Create(ctx context.Context, user *entity.User) error {
	return u.db.WithContext(ctx).Create(&user).Error
}

// Update is a method to create new record
func (u UserRepo) Update(ctx context.Context, id int, user *entity.User) error {
	return u.db.WithContext(ctx).Model(&entity.User{}).Where("id = ?", id).Updates(&user).Error
}

// GetByID is a method to retrieve single record by id
func (u UserRepo) GetByID(ctx context.Context, id int) (*entity.User, error) {
	var user entity.User
	err := u.db.WithContext(ctx).
		Joins("Role").
		Where("users.id = ?", id).
		Take(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetByUsername is a method to retrieve single record by username
func (u UserRepo) GetByUsername(ctx context.Context, username string) (*entity.User, error) {
	var user entity.User

	err := u.db.WithContext(ctx).
		Joins("Role").
		Where("users.username = ?", username).
		Take(&user).Error
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetAll is a method to retrieve all records
func (u UserRepo) GetAll(ctx context.Context) ([]entity.User, error) {
	var users []entity.User

	err := u.db.WithContext(ctx).
		Joins("Role").
		Find(&users).Error
	if err != nil {
		return nil, err
	}

	return users, nil
}

// Delete is a method to remove existing data
func (u UserRepo) Delete(ctx context.Context, id int) error {
	return u.db.WithContext(ctx).Where("id = ?", id).Delete(&entity.User{}).Error
}
