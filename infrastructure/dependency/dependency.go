package dependency

import (
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/grpc/client"
	"gitlab.com/ugrd/lsp-project/infrastructure/cache"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"gitlab.com/ugrd/lsp-project/pkg/security/jwt"
)

// Dependency is a struct
type Dependency struct {
	Cfg        *config.Config
	Repo       *registry.RepoRegistry
	Logger     *logger.Logger
	GRPCClient *client.GRPCClient
	Cache      *cache.Cache
	JWT        *jwt.JWT
}

// New is a constructor
func New(opts ...Option) *Dependency {
	dep := &Dependency{}

	for _, opt := range opts {
		opt.apply(dep)
	}

	return dep
}
