package dependency

import (
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/grpc/client"
	"gitlab.com/ugrd/lsp-project/infrastructure/cache"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"gitlab.com/ugrd/lsp-project/pkg/security/jwt"
)

// Option is an interface
type Option interface {
	apply(*Dependency)
}

// optionFunc is a type
type optionFunc func(*Dependency)

// apply is a method implementation
func (f optionFunc) apply(dep *Dependency) {
	f(dep)
}

// WithConfig is an option function
func WithConfig(cnf *config.Config) Option {
	return optionFunc(func(dep *Dependency) {
		dep.Cfg = cnf
	})
}

// WithRepository is an option function
func WithRepository(repository *registry.RepoRegistry) Option {
	return optionFunc(func(dep *Dependency) {
		dep.Repo = repository
	})
}

// WithLogger is an option function
func WithLogger(loggr *logger.Logger) Option {
	return optionFunc(func(dep *Dependency) {
		dep.Logger = loggr
	})
}

// WithJWT is an option function
func WithJWT(jwt *jwt.JWT) Option {
	return optionFunc(func(dep *Dependency) {
		dep.JWT = jwt
	})
}

// WithGRPCClient is an option function
func WithGRPCClient(gClient *client.GRPCClient) Option {
	return optionFunc(func(dep *Dependency) {
		dep.GRPCClient = gClient
	})
}

// WithCache is an option function
func WithCache(c *cache.Cache) Option {
	return optionFunc(func(dep *Dependency) {
		dep.Cache = c
	})
}
