package registry

import (
	"gitlab.com/ugrd/lsp-project/domain/repository"
	"gitlab.com/ugrd/lsp-project/infrastructure/persistence"
	"gorm.io/gorm"
)

// RepoRegistry is a struct
type RepoRegistry struct {
	UserRepo repository.UserRepositoryInterface
	RoleRepo repository.RoleRepoInterface

	DB *gorm.DB
}

// NewRepoRegistry is constructor
func NewRepoRegistry(db *gorm.DB) *RepoRegistry {
	return &RepoRegistry{
		UserRepo: persistence.NewUserRepo(db),
		RoleRepo: persistence.NewRoleRepo(db),
		DB:       db,
	}
}
