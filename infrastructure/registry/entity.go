package registry

import (
	"gitlab.com/ugrd/lsp-project/domain/contract"
	"gitlab.com/ugrd/lsp-project/domain/entity"
)

// CollectEntities is function collects entities
func CollectEntities() []contract.Entity {
	return []contract.Entity{
		{Entity: entity.Role{}},
		{Entity: entity.User{}},
	}
}

// CollectTables is function collects entity names
func CollectTables() []contract.Table {
	var role entity.Role
	var user entity.User

	return []contract.Table{
		{Name: role.TableName()},
		{Name: user.TableName()},
	}
}

// NewEntityRegistry is constructor of Registry
func NewEntityRegistry() *entity.Registry {
	var entityRegistry []contract.Entity
	var tableRegistry []contract.Table

	entityRegistry = append(entityRegistry, CollectEntities()...)
	tableRegistry = append(tableRegistry, CollectTables()...)

	return &entity.Registry{
		Entities: entityRegistry,
		Tables:   tableRegistry,
	}
}
