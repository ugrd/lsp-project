package core

import (
	"encoding/base64"
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/pkg/security/jwt"
)

// NewJWT is constructor
func NewJWT(cfg *config.Config) (*jwt.JWT, error) {
	privateKey, errPri := base64.StdEncoding.DecodeString(cfg.KeyConfig.AppPrivateKey)
	publicKey, errPub := base64.StdEncoding.DecodeString(cfg.KeyConfig.AppPublicKey)

	if errPub != nil {
		return nil, errPri
	}

	if errPub != nil {
		return nil, errPub
	}

	j := jwt.NewJWT(privateKey, publicKey)

	return j, nil
}
