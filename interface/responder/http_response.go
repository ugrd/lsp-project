package responder

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// HTTPResponse is a struct
type HTTPResponse struct {
	gin     *gin.Context
	code    int
	message string
	data    interface{}
}

// HTTPOutput is a struct
type HTTPOutput struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// NewHTTPResponse is a constructor
func NewHTTPResponse(c *gin.Context, msg string) *HTTPResponse {
	return &HTTPResponse{
		gin:     c,
		message: msg,
	}
}

// WithHTTPStatus is a method
func (r *HTTPResponse) WithHTTPStatus(httpStatus int) *HTTPResponse {
	r.code = httpStatus

	return r
}

// WithData is a method
func (r *HTTPResponse) WithData(data interface{}) *HTTPResponse {
	r.data = data

	return r
}

// JSON is a method
func (r *HTTPResponse) JSON() {
	rsp := HTTPOutput{
		Code:    http.StatusText(r.code),
		Message: r.message,
		Data:    r.data,
	}

	r.gin.Header("Accept-Language", "en")
	r.gin.JSON(r.code, rsp)
}
