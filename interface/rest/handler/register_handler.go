package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/lsp-project/interface/exception"
	"gitlab.com/ugrd/lsp-project/interface/responder"
	"gitlab.com/ugrd/lsp-project/interface/rest/errs"
	"gitlab.com/ugrd/lsp-project/interface/rest/service/auth"
	"gitlab.com/ugrd/lsp-project/utils"
	"net/http"
	"regexp"
)

func (hdl *Handler) Register(c *gin.Context) {
	var req RegisterRequest
	if err := c.ShouldBind(&req); err != nil {
		exception.NewHTTPError(c, err.Error()).
			WithHTTPStatus(http.StatusBadRequest).
			JSON()
		return
	}

	if err := req.Validate(); err != nil {
		exception.NewHTTPError(c, exception.ErrorUnprocessableEntity.Error()).
			WithHTTPStatus(http.StatusUnprocessableEntity).
			WithErrFieldsFromMap(utils.ErrToMap(err)).
			JSON()
	}

	resp, err := hdl.service.Register.Handle(c, &auth.RegisterPayload{
		Name:     req.Name,
		Username: req.Username,
		Password: req.Password,
	})

	if err != nil {
		switch err {
		case errs.ErrDuplicateEntity:
			exception.NewHTTPError(c, err.Error()).
				WithHTTPStatus(http.StatusBadRequest).
				JSON()
			return
		default:
			exception.NewHTTPError(c, err.Error()).
				WithHTTPStatus(http.StatusInternalServerError).
				JSON()
			return
		}
	}

	responder.NewHTTPResponse(c, fmt.Sprintf("OK")).
		WithHTTPStatus(http.StatusOK).
		WithData(resp).
		JSON()
	return
}

// RegisterRequest is a struct
type RegisterRequest struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate is a method to validate
func (u RegisterRequest) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Name, validation.Required, validation.Length(3, 100)),
		validation.Field(
			&u.Username,
			validation.Required,
			validation.Length(6, 101),
			validation.Match(regexp.MustCompile("^[A-Za-z][A-Za-z0-9_]{6,29}$")).Error("Must be alpha-numeric, can include underscore"),
		),
		validation.Field(
			&u.Password,
			validation.Required,
			validation.Length(8, 255),
		),
	)
}
