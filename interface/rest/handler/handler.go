package handler

import (
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
	"gitlab.com/ugrd/lsp-project/interface/rest/service"
)

// Handler is a struct acts as an object that hold
// all handlers of rest handler
type Handler struct {
	dep     *dependency.Dependency
	service *service.Service
}

// NewHandler is a constructor of Handler instance
// requires *dependency.Dependency parameter
func NewHandler(dep *dependency.Dependency) *Handler {
	return &Handler{
		dep:     dep,
		service: service.New(dep),
	}
}
