package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/ugrd/lsp-project/interface/responder"
	"net/http"
)

// Ping is a method to handle
func (hdl *Handler) Ping(c *gin.Context) {
	responder.NewHTTPResponse(c, fmt.Sprintf("OK")).
		WithHTTPStatus(http.StatusOK).
		WithData(map[string]interface{}{
			"message": "PONG!!",
		}).
		JSON()

	return
}
