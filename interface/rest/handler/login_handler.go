package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/ugrd/lsp-project/interface/exception"
	"gitlab.com/ugrd/lsp-project/interface/responder"
	"gitlab.com/ugrd/lsp-project/interface/rest/errs"
	"gitlab.com/ugrd/lsp-project/interface/rest/service/auth"
	"gitlab.com/ugrd/lsp-project/utils"
	"net/http"
)

// Login is a method
func (hdl *Handler) Login(c *gin.Context) {
	var req LoginRequest
	if err := c.ShouldBind(&req); err != nil {
		exception.NewHTTPError(c, err.Error()).
			WithHTTPStatus(http.StatusBadRequest).
			JSON()
		return
	}

	if err := req.Validate(); err != nil {
		exception.NewHTTPError(c, exception.ErrorUnprocessableEntity.Error()).
			WithHTTPStatus(http.StatusUnprocessableEntity).
			WithErrFieldsFromMap(utils.ErrToMap(err)).
			JSON()
		return
	}

	resp, err := hdl.service.Login.Handle(c, &auth.LoginPayload{
		Username: req.Username,
		Password: req.Password,
	})

	if err != nil {
		switch err {
		case errs.ErrNotFound, errs.ErrUnauthorized:
			exception.NewHTTPError(c, exception.ErrorUnauthorized.Error()).
				WithHTTPStatus(http.StatusUnauthorized).
				JSON()
			return
		default:
			exception.NewHTTPError(c, exception.ErrorInternalServerError.Error()).
				WithHTTPStatus(http.StatusInternalServerError).
				JSON()
			return
		}
	}

	responder.NewHTTPResponse(c, fmt.Sprintf("OK")).
		WithHTTPStatus(http.StatusOK).
		WithData(resp).
		JSON()
	return
}

// LoginRequest is a struct defines login payload
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Validate is function to validate AuthPayload
func (a LoginRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, validation.Required, validation.Length(6, 100)),
		validation.Field(&a.Password, validation.Required, validation.Length(8, 70)),
	)
}
