package errs

import "errors"

var (
	// ErrNotFound is an error
	ErrNotFound = errors.New("err.service.not_found")
	// ErrUnknown is an error
	ErrUnknown = errors.New("err.service.unknown")
	// ErrUnauthorized is an error
	ErrUnauthorized = errors.New("err.service.unauthorized")
	// ErrDuplicateEntity
	ErrDuplicateEntity = errors.New("err.service.duplicate_entity")
)
