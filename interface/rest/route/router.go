package route

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
	"gitlab.com/ugrd/lsp-project/interface/rest/handler"
	"gitlab.com/ugrd/lsp-project/interface/rest/middleware"
)

// Router is a struct contains dependencies needed
type Router struct {
	*dependency.Dependency
}

// NewRouter is a constructor will initialize Router.
func NewRouter(options ...Option) *Router {
	router := &Router{}

	for _, opt := range options {
		opt(router)
	}

	return router
}

// Init is a function initialize router
func (r *Router) Init() *gin.Engine {

	if !r.Cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	e := gin.Default()
	e.Use(cors.Default())
	e.Use(middleware.Logger())

	hdl := handler.NewHandler(r.Dependency)

	v1 := e.Group("/api/v1")

	v1.GET("/ping", hdl.Ping)

	// auth
	v1.POST("/login", hdl.Login)
	v1.POST("register", hdl.Register)

	return e
}
