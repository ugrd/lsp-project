package auth

import (
	"context"
	"fmt"
	"gitlab.com/ugrd/lsp-project/domain/constant"
	"gitlab.com/ugrd/lsp-project/infrastructure/cache"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/interface/rest/errs"
	"gitlab.com/ugrd/lsp-project/pkg/security"
	"gitlab.com/ugrd/lsp-project/pkg/security/jwt"
	"gorm.io/gorm"
	"time"
)

// LoginService is a struct
type LoginService struct {
	repo  *registry.RepoRegistry
	jwt   *jwt.JWT
	cache *cache.Cache
}

// LoginPayload is a struct
type LoginPayload struct {
	Username string `db:"username" json:"username"`
	Password string `db:"password" json:"password"`
}

// LoginResponse is a struc
type LoginResponse struct {
	AccessToken string `json:"access_token"`
	ExpiredIn   int64  `json:"expired_in"`
}

// NewLoginService is a constructor
func NewLoginService(repo *registry.RepoRegistry, jwt *jwt.JWT, cache *cache.Cache) *LoginService {
	return &LoginService{
		repo:  repo,
		jwt:   jwt,
		cache: cache,
	}
}

// Handle is a method
func (a *LoginService) Handle(ctx context.Context, payload *LoginPayload) (*LoginResponse, error) {
	r, err := a.repo.UserRepo.GetByUsername(ctx, payload.Username)
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, errs.ErrNotFound
		default:
			return nil, errs.ErrUnknown
		}
	}

	if !security.HashVerify(payload.Password, r.Password) {
		return nil, errs.ErrUnauthorized
	}

	ttl := time.Hour * 24

	claim := map[string]string{
		"x-code": r.Username,
		"access": r.Role.Name,
	}

	token, err := a.jwt.Create(ttl, claim)
	if err != nil {
		return nil, errs.ErrUnknown
	}

	cacheKey := fmt.Sprintf(constant.AuthCacheKey, r.ID)

	err = a.cache.Set(ctx, cacheKey, token, ttl)
	if err != nil {
		return nil, errs.ErrUnknown
	}

	return &LoginResponse{
		AccessToken: token,
		ExpiredIn:   int64(ttl),
	}, nil
}
