package auth

import (
	"context"
	"gitlab.com/ugrd/lsp-project/domain/entity"
	"gitlab.com/ugrd/lsp-project/infrastructure/cache"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/interface/rest/errs"
	"gitlab.com/ugrd/lsp-project/pkg/security"
	"gorm.io/gorm"
)

// RegisterService is a struct
type RegisterService struct {
	repo  *registry.RepoRegistry
	cache *cache.Cache
}

// RegisterPayload is a struct
type RegisterPayload struct {
	Name     string `json:"name" db:"name"`
	Username string `db:"username" json:"username"`
	Password string `db:"password" json:"password"`
}

// RegisterResponse is a struc
type RegisterResponse struct {
	Name     string `json:"name" db:"name"`
	Username string `db:"username" json:"username"`
}

// NewRegisterService is a constructor
func NewRegisterService(repo *registry.RepoRegistry, cache *cache.Cache) *RegisterService {
	return &RegisterService{repo: repo, cache: cache}
}

// Handle is a method
func (a *RegisterService) Handle(ctx context.Context, payload *RegisterPayload) (*RegisterResponse, error) {
	r, err := a.repo.UserRepo.GetByUsername(ctx, payload.Username)
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
		default:
			return nil, errs.ErrUnknown
		}
	}

	if r != nil {
		return nil, errs.ErrDuplicateEntity
	}

	err = a.repo.UserRepo.Create(ctx, &entity.User{
		Name:     payload.Name,
		Username: payload.Username,
		Password: security.HashMake(payload.Password),
	})

	if err != nil {
		return nil, errs.ErrUnknown
	}

	return &RegisterResponse{
		Name:     payload.Name,
		Username: payload.Username,
	}, nil
}
