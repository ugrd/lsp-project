package user

import "gitlab.com/ugrd/lsp-project/infrastructure/registry"

// UserService is a struct
type UserService struct {
	repo *registry.RepoRegistry
}

// New is a constructor
func New(repo *registry.RepoRegistry) *UserService {
	return &UserService{
		repo: repo,
	}
}
