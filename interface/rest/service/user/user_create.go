package user

import (
	"context"
	"gitlab.com/ugrd/lsp-project/domain/entity"
	"gitlab.com/ugrd/lsp-project/interface/rest/errs"
	"gorm.io/gorm"
)

// Create is a method
func (u *UserService) Create(ctx context.Context, user *entity.User) (*entity.User, error) {
	err := u.repo.UserRepo.Create(ctx, user)
	if err != nil {
		return nil, errs.ErrUnknown
	}

	r, _ := u.repo.UserRepo.GetByID(ctx, int(user.ID))

	return r, nil
}

// GetByID is a method
func (u *UserService) GetByID(ctx context.Context, id int64) (*entity.User, error) {
	r, err := u.repo.UserRepo.GetByID(ctx, int(id))
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			return nil, errs.ErrNotFound
		default:
			return nil, errs.ErrUnknown
		}
	}

	return r, nil
}
