package service

import (
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
	"gitlab.com/ugrd/lsp-project/interface/rest/service/auth"
	"gitlab.com/ugrd/lsp-project/interface/rest/service/user"
)

// Service is a struct
type Service struct {
	User     *user.UserService
	Login    *auth.LoginService
	Register *auth.RegisterService
}

// New is a constructor
func New(dep *dependency.Dependency) *Service {
	return &Service{
		User:     user.New(dep.Repo),
		Login:    auth.NewLoginService(dep.Repo, dep.JWT, dep.Cache),
		Register: auth.NewRegisterService(dep.Repo, dep.Cache),
	}
}
