package cmd

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/ugrd/lsp-project/grpc/server"
	"gitlab.com/ugrd/lsp-project/infrastructure/persistence"
	"gitlab.com/ugrd/lsp-project/infrastructure/seeder"
	"gitlab.com/ugrd/lsp-project/pkg/security"
)

// newGRPCServer is a method command cli to run grpc
func (cmd *Command) newGRPCServer() *cli.Command {
	return &cli.Command{
		Name:  "grpc:start",
		Usage: "A command to run gRPC server",
		Action: func(c *cli.Context) error {
			grpcServer := server.NewGRPCServer(
				cmd.Dependency.Cfg,
				cmd.Dependency.Repo,
				cmd.Dependency.Logger,
			)
			err := grpcServer.Run(cmd.Dependency.Cfg.GRPCPort)
			if err != nil {
				return err
			}

			return nil
		},
	}
}

// newDBMigrate is a method command cli to run db migration
func (cmd *Command) newDBMigrate() *cli.Command {
	return &cli.Command{
		Name:  "db:migrate",
		Usage: "A command to run database migration",
		Action: func(c *cli.Context) error {
			db, errConn := persistence.NewDBConnection(cmd.Dependency.Cfg)
			if errConn != nil {
				return fmt.Errorf("unable to connect to database: %w", errConn)
			}

			err := persistence.AutoMigrate(db)
			if err != nil {
				return fmt.Errorf("cannot run auto migrate: %w", err)
			}

			return nil
		},
	}
}

// newMakeSecret is a method command cli to create public & private key
func (cmd *Command) newMakeSecret() *cli.Command {
	return &cli.Command{
		Name:  "create:secret",
		Usage: "A command to create secret key and public key",
		Action: func(c *cli.Context) error {
			secret, err := security.GenerateSecretKey()

			if err != nil {
				return err
			}

			fmt.Println("APP_PRIVATE_KEY:")
			fmt.Println(secret.PrivateKey)
			fmt.Println("APP_PUBLIC_KEY:")
			fmt.Println(secret.PublicKey)

			return nil
		},
	}
}

// newDBSeed is a method command cli to run db seeder
func (cmd *Command) newDBSeed() *cli.Command {
	return &cli.Command{
		Name:  "db:seed",
		Usage: "A command to run database seeder",
		Action: func(c *cli.Context) error {
			seed := seeder.NewSeeder(cmd.Dependency.Repo)
			err := seed.Seed(
				&seeder.RoleSeeder{},
				&seeder.UserSeeder{},
			)
			if err != nil {
				return fmt.Errorf("cannot run db seeder: %w", err)
			}

			return nil
		},
	}
}
