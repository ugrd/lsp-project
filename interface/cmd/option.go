package cmd

import (
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
)

// Option is an option type
type Option func(c *Command)

// WithDependency is a function option
func WithDependency(dep *dependency.Dependency) Option {
	return func(c *Command) {
		c.Dependency = dep
	}
}
