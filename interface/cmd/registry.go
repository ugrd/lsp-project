package cmd

import "github.com/urfave/cli/v2"

// Build is a method
func (cmd *Command) Build() []*cli.Command {
	cmd.registerCLI(cmd.newGRPCServer())
	cmd.registerCLI(cmd.newDBMigrate())
	cmd.registerCLI(cmd.newMakeSecret())
	cmd.registerCLI(cmd.newDBSeed())

	return cmd.CLI
}
