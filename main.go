package main

import (
	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/infrastructure/cache"
	"gitlab.com/ugrd/lsp-project/infrastructure/core"
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
	"gitlab.com/ugrd/lsp-project/infrastructure/persistence"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/interface/cmd"
	"gitlab.com/ugrd/lsp-project/interface/rest"
	"gitlab.com/ugrd/lsp-project/interface/rest/route"
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"log"
	"os"
	"strconv"
	"time"
)

// main is a main function
func main() {
	if errEnv := godotenv.Load(); errEnv != nil {
		log.Fatal("Error loading .env file")
	}

	conf := config.New()

	db, errConn := persistence.NewDBConnection(conf)
	if errConn != nil {
		log.Fatalf("unable connect to database, %v", errConn)
	}

	jwt, errJWT := core.NewJWT(conf)
	if errJWT != nil {
		log.Fatalf("unable to initialize JWT, err: %v", errJWT)
	}

	redisDB, errRedis := cache.NewRedisCache(conf)
	if errRedis != nil {
		log.Fatalf("unable to connect to redis, err: %v", errRedis)
	}

	cacheDB := cache.New(redisDB)

	repo := registry.NewRepoRegistry(db)
	loggr := logger.New(logger.NewConfig())

	command := cmd.NewCommand(
		cmd.WithDependency(dependency.New(
			dependency.WithConfig(conf),
			dependency.WithRepository(repo),
			dependency.WithLogger(loggr),
			dependency.WithJWT(jwt),
		)),
	)

	app := cmd.NewCLI()
	app.Commands = command.Build()

	app.Action = func(ctx *cli.Context) error {
		router := route.NewRouter(
			route.WithDependency(dependency.New(
				dependency.WithConfig(conf),
				dependency.WithRepository(repo),
				dependency.WithLogger(loggr),
				dependency.WithJWT(jwt),
				dependency.WithCache(cacheDB),
			)),
		).Init()

		shutdownTimeout := 10 * time.Second

		err := rest.RunHTTPServer(router, strconv.Itoa(conf.AppPort), shutdownTimeout)
		if err != nil {
			return err
		}

		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatalf("Unable to run CLI command, err: %v", err)
	}
}
