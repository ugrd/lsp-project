package client_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/lsp-project/grpc/client"
	"testing"
)

func TestRun(t *testing.T) {
	err := client.Run()

	assert.NoError(t, err)
}
