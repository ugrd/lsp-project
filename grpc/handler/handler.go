package handler

import (
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/infrastructure/registry"
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"gitlab.com/ugrd/lsp-project/proto/foo"
)

// Interface is an interface
type Interface interface {
	foo.UserServiceServer
	foo.HelloServer
	foo.AuthServer
	foo.LogServiceServer
}

// Dependency collects dependencies needed by handler
type Dependency struct {
	Config *config.Config
	Repo   *registry.RepoRegistry
	Logger *logger.Logger
}

// CoreGRPCService collects grpc service server
type CoreGRPCService struct {
	foo.UnimplementedUserServiceServer
	foo.UnimplementedHelloServer
	foo.UnimplementedAuthServer
	foo.UnimplementedLogServiceServer
}

// Handler is struct
type Handler struct {
	Dependency *Dependency

	CoreGRPCService
}

// NewHandler is a constructor
func NewHandler(conf *config.Config, repo *registry.RepoRegistry, loggr *logger.Logger) *Handler {
	return &Handler{
		Dependency: &Dependency{
			Config: conf,
			Repo:   repo,
			Logger: loggr,
		},
	}
}

var _ Interface = &Handler{}
