package cryptox_test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/lsp-project/pkg/cryptox"
	"testing"
)

func TestMakeBlindIndex(t *testing.T) {
	data := "087750676800"
	r, err := cryptox.MakeBlindIndex(data)

	assert.NoError(t, err)
	assert.NotEmpty(t, r)
	fmt.Println(r)

	data2 := "Hello world, today is a great day for coding"

	r, _ = cryptox.MakeBlindIndex(data2)
	fmt.Println(r)
}
