package storagex_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ugrd/lsp-project/pkg/storagex"
	"gitlab.com/ugrd/lsp-project/tests"
	"testing"
)

func TestNewMinio(t *testing.T) {
	textbox := tests.Init()

	r, err := storagex.NewMinio(textbox.Cfg)

	assert.NoError(t, err)
	assert.NotNil(t, r)
}
