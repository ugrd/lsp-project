package logger_test

import (
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"testing"
)

func TestNew(t *testing.T) {
	loggr := logger.New(logger.NewConfig())
	loggr.Log.Info("hello there!")
}
