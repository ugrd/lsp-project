package entity

import (
	"gitlab.com/ugrd/lsp-project/domain/contract"
	"time"

	"gorm.io/gorm"
)

// User is a struct
type User struct {
	ID        uint64    `gorm:"not null;uniqueIndex;primaryKey" json:"id"`
	RoleID    uint64    `gorm:"column:role_id; not null;" json:"role_id"`
	Name      string    `gorm:"size: 100;not null;" json:"name"`
	Username  string    `gorm:"size: 100;null;uniqueIndex" json:"username"`
	Password  string    `gorm:"size: 100;null" json:"password"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt gorm.DeletedAt
	Role      Role `json:"role"`
}

// TableName is a method
func (u User) TableName() string {
	return "users"
}

// FilterableFields is a method
func (u User) FilterableFields() []interface{} {
	return []interface{}{"id", "name", "username"}
}

// TimeFields is a method
func (u User) TimeFields() []interface{} {
	return []interface{}{"created_at", "updated_at", "deleted_at"}
}

var _ contract.EntityInterface = &User{}
