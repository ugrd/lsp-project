package repository

import (
	"context"
	"gitlab.com/ugrd/lsp-project/domain/entity"
)

// RoleRepoInterface is role repo contract
type RoleRepoInterface interface {
	Create(ctx context.Context, role *entity.Role) error
	Update(ctx context.Context, role *entity.Role, id uint) error
	Delete(ctx context.Context, id uint) error
	GetByID(ctx context.Context, id uint) (*entity.Role, error)
	GetByCode(ctx context.Context, code string) (*entity.Role, error)
	GetAll(ctx context.Context) ([]*entity.Role, error)
}
