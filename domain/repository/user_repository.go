package repository

import (
	"context"
	"gitlab.com/ugrd/lsp-project/domain/entity"
)

// UserRepositoryInterface is an interface
type UserRepositoryInterface interface {
	Create(ctx context.Context, user *entity.User) error
	Update(ctx context.Context, id int, user *entity.User) error
	GetByID(ctx context.Context, id int) (*entity.User, error)
	GetByUsername(ctx context.Context, username string) (*entity.User, error)
	GetAll(ctx context.Context) ([]entity.User, error)
	Delete(ctx context.Context, id int) error
}
