package constant

// ACCESS is a type
type ACCESS uint8

const (
	User ACCESS = iota
	Admin
)

var (
	// accesses is a list of accesses
	accesses = []string{
		"USER",
		"ADMIN",
	}
)

// String is a method
func (ac ACCESS) String() string {
	return accesses[ac]
}
