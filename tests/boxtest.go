package tests

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/ugrd/lsp-project/config"
	"gitlab.com/ugrd/lsp-project/domain/service"
	"gitlab.com/ugrd/lsp-project/infrastructure/dependency"
	"gitlab.com/ugrd/lsp-project/infrastructure/persistence"
	"gitlab.com/ugrd/lsp-project/pkg/logger"
	"gitlab.com/ugrd/lsp-project/tests/database"
	"gitlab.com/ugrd/lsp-project/utils"
	"log"
	"os"
)

type BoxTest struct {
	Ctx context.Context
	*dependency.Dependency
}

// Init is a function to initialize tests
func Init() *BoxTest {
	errT := os.Setenv("TEST_MODE", "true")
	if errT != nil {
		log.Fatalf("unable to set test mode, err: %s", errT.Error())
	}

	if err := godotenv.Load(fmt.Sprintf("%s/.env", utils.RootDir())); err != nil {
		log.Fatalf("no .env file provided.")
	}

	cfg := config.New()
	ctx := context.Background()

	db, errConn := persistence.NewDBConnection(cfg)
	if errConn != nil {
		log.Fatalf("unable connect to database, %v", errConn)
	}

	drop := database.NewDrop(db)
	errDrop := drop.Reset(ctx)
	if errDrop != nil {
		log.Fatalf("err drop: %v", errDrop)
	}

	repo := service.NewDBService(db)
	loggr := logger.New(logger.NewConfig())

	return &BoxTest{
		Ctx: ctx,
		Dependency: dependency.New(
			dependency.WithConfig(cfg),
			dependency.WithRepository(repo),
			dependency.WithLogger(loggr),
		),
	}
}
