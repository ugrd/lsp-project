package utils

import "encoding/json"

// ErrToMap is a function to convert
func ErrToMap(err error) map[string]interface{} {
	val, _ := json.Marshal(err)
	res := make(map[string]interface{})

	_ = json.Unmarshal(val, &res)

	return res
}
